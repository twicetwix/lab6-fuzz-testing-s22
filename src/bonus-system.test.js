import { calculateBonuses } from "./bonus-system.js";
import {describe, test} from "@jest/globals";
const assert = require("assert")

const STANDARD_MULTIPLIER = 0.05
const PREMIUM_MULTIPLIER = 0.1
const DIAMOND_MULTIPLIER = 0.2

const STANDARD_PROGRAM = "Standard"
const PREMIUM_PROGRAM = "Premium"
const DIAMOND_PROGRAM = "Diamond"
const LOLKEK_PROGRAM = "lolkek"

describe('Bonus system tests', () => {
    let app;
    // standard multiplier
    test('Interval mid 0-10000 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 5000;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 1);
        done();
    });

    test('Boundary 9999 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 9999;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 1);
        done();
    });

    test('Boundary 10000 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 10000;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 1.5);
        done();
    });

    test('Interval mid 10000-50000 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 25000;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 1.5);
        done();
    });

    test('Boundary 49999 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 49999;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 1.5);
        done();
    });

    test('Boundary 50000 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 2);
        done();
    });

    test('Interval mid 50000-100000 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 2);
        done();
    });

    test('Boundary 99999 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 99999;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 2);
        done();
    });

    test('Boundary 100000 (Standard)',  (done) => {
        let program = STANDARD_PROGRAM;
        let amount = 100000;
        expect(calculateBonuses(program, amount)).toEqual(STANDARD_MULTIPLIER * 2.5);
        done();
    });

    // Premium multiplier
    test('Interval mid 0-10000 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 5000;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 1);
        done();
    });

    test('Boundary 9999 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 9999;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 1);
        done();
    });

    test('Boundary 10000 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 10000;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 1.5);
        done();
    });

    test('Interval mid 10000-50000 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 25000;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 1.5);
        done();
    });

    test('Boundary 49999 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 49999;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 1.5);
        done();
    });

    test('Boundary 50000 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 2);
        done();
    });

    test('Interval mid 50000-100000 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 2);
        done();
    });

    test('Boundary 99999 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 99999;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 2);
        done();
    });

    test('Boundary 100000 (Premium)',  (done) => {
        let program = PREMIUM_PROGRAM;
        let amount = 100000;
        expect(calculateBonuses(program, amount)).toEqual(PREMIUM_MULTIPLIER * 2.5);
        done();
    });

    // Diamond multiplier
    test('Interval mid 0-10000 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 5000;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 1);
        done();
    });

    test('Boundary 9999 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 9999;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 1);
        done();
    });

    test('Boundary 10000 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 10000;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 1.5);
        done();
    });

    test('Interval mid 10000-50000 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 25000;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 1.5);
        done();
    });

    test('Boundary 49999 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 49999;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 1.5);
        done();
    });

    test('Boundary 50000 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 2);
        done();
    });

    test('Interval mid 50000-100000 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 2);
        done();
    });

    test('Boundary 99999 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 99999;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 2);
        done();
    });

    test('Boundary 100000 (Diamond)',  (done) => {
        let program = DIAMOND_PROGRAM;
        let amount = 100000;
        expect(calculateBonuses(program, amount)).toEqual(DIAMOND_MULTIPLIER * 2.5);
        done();
    });

    // Trash multiplier
    test('Interval mid 0-10000 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 5000;
        expect(calculateBonuses(program, amount)).toEqual(0 * 1);
        done();
    });

    test('Boundary 9999 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 9999;
        expect(calculateBonuses(program, amount)).toEqual(0 * 1);
        done();
    });

    test('Boundary 10000 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 10000;
        expect(calculateBonuses(program, amount)).toEqual(0 * 1.5);
        done();
    });

    test('Interval mid 10000-50000 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 25000;
        expect(calculateBonuses(program, amount)).toEqual(0 * 1.5);
        done();
    });

    test('Boundary 49999 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 49999;
        expect(calculateBonuses(program, amount)).toEqual(0 * 1.5);
        done();
    });

    test('Boundary 50000 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(0 * 2);
        done();
    });

    test('Interval mid 50000-100000 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 50000;
        expect(calculateBonuses(program, amount)).toEqual(0 * 2);
        done();
    });

    test('Boundary 99999 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 99999;
        expect(calculateBonuses(program, amount)).toEqual(0 * 2);
        done();
    });

    test('Boundary 100000 (Diamond)',  (done) => {
        let program = LOLKEK_PROGRAM;
        let amount = 100000;
        expect(calculateBonuses(program, amount)).toEqual(0 * 2.5);
        done();
    });

})